const mongoose = require('mongoose');
const util = require('util');

// config should be imported before importing any other setup file
const config = require('./config/config');
const app = require('./config/express');

// make bluebird default Promise
Promise = require('bluebird');

// plugin bluebird promise in mongoose
mongoose.Promise = Promise;

const mongoUri = config.mongo.host;

const http = require('http').Server(app);

mongoose.connect(mongoUri, {
  keepAlive: 1,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});
mongoose.connection.on('error', () => {
  throw new Error(`unable to connect to database: ${mongoUri}`);
});

// print mongoose logs in dev env
if (config.mongooseDebug) {
  mongoose.set('debug', (collectionName, method, query, doc) => {
    debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
  });
}

const port = config.env === 'test' ? '4041' : config.port;
http.listen(port, () => {
  console.info(`
    -----------------------------------------
    server started on port ${port} (${config.env})
    -----------------------------------------
    `);
});

module.exports = app;
