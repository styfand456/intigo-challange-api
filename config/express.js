const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compress = require('compression');
const cors = require('cors');
const httpStatus = require('http-status');
const expressValidation = require('express-validation');
const helmet = require('helmet');
const flash = require('express-flash');
const config = require('./config');
const APIError = require('../server/helpers/APIError');
const routes = require('../server/routes/index.route');

const app = express();

// parse body params and attache them to req.body
app.use(bodyParser.json({
  limit: '45mb',
}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '45mb',
}));

app.use(cookieParser());
app.use(compress());

// set the view engine to ejs
app.set('view engine', 'ejs');

// To define flash message and render it
app.use(flash());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// mount all routes on /api path
app.use('/api', routes);

// if error is not an instanceOf APIError, convert it.
app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    // validation error contains errors which is an array of error each containing message[]
    const unifiedErrorMessage = err.errors
      .map(error => error.messages.join('. '))
      .join(' and ');
    const error = new APIError(unifiedErrorMessage, err.status, true);
    return next(error);
  } else if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }
  return next(err);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND);
  return next(err);
});

// error handler, send stacktrace only during development
app.use((
  err,
  req,
  res,
  next
) => {
  config.env === 'development' &&
    console.error(err);
  res.status(err.status).json({
    success: false,
    message: err.isPublic ? err.message : httpStatus[err.status],
    stack: config.env === 'development' ? err.stack : {},
  });
});

module.exports = app;
