const Joi = require('joi');

module.exports = {
  // post /api/auth/signup
  signup: {
    body: {
      firstname: Joi.string().min(2).max(15).required(),
      lastname: Joi.string().min(2).max(15).required(),
      email: Joi.string().email().required(),
      role: Joi.strict().valid('USER', 'ADMIN'),
      password: Joi.string().min(6).required(),
      passwordRepeat: Joi.string().valid(Joi.ref('password')).required(),
    },
  },

  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },
  },

  // PUT /api/users/:userId
  updateUser: {
    params: {
      userId: Joi.string().hex().required(),
    },
    body: {
      firstname: Joi.string().min(2).max(15),
      lastname: Joi.string().min(2).max(15),
      email: Joi.string().email(),
      role: Joi.strict().valid('USER', 'ADMIN'),
    },
  },

  // PUT /api/users
  removeManyUsers: {
    body: {
      users: Joi.array().items(Joi.string()),
    },
  },

  // PATCH /api/users/:userId/password
  changePassword: {
    params: {
      userId: Joi.string().hex().required(),
    },
    body: {
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().min(6).required(),
      newPasswordRepeated: Joi.string()
        .valid(Joi.ref('newPassword'))
        .required(),
    },
  },
};
