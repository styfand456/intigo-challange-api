// custom errors code for better client error handling
const errors = {
  AUTH_WRONG_CREDENTIALS: {
    code: 1001,
    message: 'Authentication failed. Wrong credentials'
  },
  AUTH_NO_PASSWORD: {
    code: 1002,
    message: 'Authentication failed. User do not have password yet'
  },
  AUTH_INVALID_TOKEN: {
    code: 1003,
    message: 'Invalid Token'
  },
  USER_DONT_EXIST: {
    code: 1004,
    message: 'No such user exists!'
  },
  MAIL_ALREADY_EXISTS: {
    code: 1005,
    message: 'Registration Failed. email already exists'
  },
  USER_ALREADY_EXIST: {
    code: 1006,
    message: 'User account already exist.'
  },
  WRONG_OLD_PASSWORD: {
    code: 1007,
    message: 'Wrong old password'
  },
};

module.exports = errors;
