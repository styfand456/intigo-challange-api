const Joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string()
    .allow(['development', 'production', 'test', 'provision', 'demo', 'local'])
    .default('development'),
  PORT: Joi.number().default(4042),
  HOST: Joi.string().description('Host url').default('127.0.0.1'),
  MONGO_HOST: Joi.string()
    .required()
    .when('NODE_ENV', {
      is: Joi.string().equal('test_local' || 'test'),
      then: Joi.string().default('mongodb://localhost/intigo-db'),
    })
    .description('Mongo DB host url'),
  MONGO_PORT: Joi.default(27017),
  REDIS_HOST: Joi.string().required().description('host adresse for redis'),
  REDIS_PORT: Joi.string().required().description('port number for redis'),
})
  .unknown()
  .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  host: envVars.HOST,
  jwtSecret: envVars.JWT_SECRET,
  weatherApiHost: envVars.WEATHER_API_HOST,
  weatherApiKey: envVars.WEATHER_API_KEY,
  mongo: {
    host: ((envVars.NODE_ENV !== 'production')
      ? 'mongodb://localhost' : 'mongodb://mongo') + envVars.MONGO_HOST,
    port: envVars.MONGO_PORT,
  },
  redis: {
    host: (envVars.NODE_ENV !== 'production')
      ? 'localhost' : envVars.REDIS_HOST,
    port: envVars.REDIS_PORT,
  },
};

module.exports = config;
