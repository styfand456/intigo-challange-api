const Promise = require('bluebird');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const bcrypt = require('bcrypt');
const mongooseHidden = require('mongoose-hidden')({
	defaultHidden: { password: true }
});

/**
 * salt used for crypting user password
 */
const SALT_WORK_FACTOR = 10;

/**
 * User Schema
 * firstname: user firstname
 * lastname: user lastname
 * email: user email (unique)
 * password: user password
 * createdAt: the date of document creation
 */
const UserSchema = new mongoose.Schema(
	{
		firstname: {
			type: String,
			required: true
		},
		lastname: {
			type: String,
			required: false
		},
		email: {
			type: String,
			unique: true
		},
		role: {
			type: String,
			enum: ['USER', 'ADMIN'],
			default: 'USER'
		},
		password: {
			type: String
		},
		resetPasswordToken: {
			type: String
		},
		resetPasswordExpires: {
			type: Date
		},
		createdAt: {
			type: Date,
			default: Date.now
		}
	},
	{
		usePushEach: true
	}
);

/**
 * Pre Save Middleware for crypting user password
 */
UserSchema.pre('save', function (next) {
	const user = this;
	if (user.password) {
		// only hash the password if it has been modified (or is new)
		if (!user.isModified('password')) return next();
		// generate a salt
		bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
			if (err) return next(err);

			// hash the password using our new salt
			bcrypt.hash(user.password, salt, function (error, hash) {
				if (error) return next(error);

				// override the cleartext password with the hashed one
				user.password = hash;
				return next();
			});
		});
	} else {
		return next();
	}
});

/**
 * UserSchema compare crypted password with binary one
 */
UserSchema.methods.comparePassword = function (candidatePassword, next) {
	bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
		if (err) return next(err);
		return next(null, isMatch);
	});
};

UserSchema.methods.comparePasswordAsync = async function (condidatePassword) {
	try {
		const match = await bcrypt.compare(condidatePassword, this.password);
		return match;
	} catch (e) {
		return e;
	}
};

UserSchema.set('toJSON', { getters: true, virtuals: true });
UserSchema.plugin(mongooseHidden);
/**
 * Statics
 */
UserSchema.statics = {
	/**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, Error>}
   */
	get(id) {
		return this.findById(id).exec().then((user) => {
			if (user) {
				return user;
			}
			const err = new Error('No such user exists!_' + httpStatus.NOT_FOUND);
			return Promise.reject(err);
		});
	},

	/**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @param {array<String>} roles - Roles to find in
   * @returns {Promise<User[]>}
   */
	list({ skip = 0, limit = 20, roles } = {}) {
		return this.find({
			role: {
				$in: roles || ['USER']
			}
		})
			.sort({
				createdAt: -1
			})
			.skip(+skip)
			.limit(+limit)
			.exec();
	}
};

/**
 * @typedef User
 */
module.exports = mongoose.model('User', UserSchema);
