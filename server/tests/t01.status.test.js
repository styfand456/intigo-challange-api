const mongoose = require('mongoose');
const request = require('supertest-as-promised');
const httpStatus = require('http-status');
const chai = require('chai');
const expect = chai.expect;
const app = require('../../index');
const config = require('../../config/config');

chai.config.includeStack = true;

after((done) => {
  // Drop database after tests end
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.db.dropDatabase(() => {
    mongoose.connection.close(() => {
      done();
    });
  });
});

describe('## Api status test', () => {
  describe('# GET /api/health-check', () => {
    it('should return positive response', (done) => {
      request(app)
        .get('/api/health-check')
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.env).to.equal(config.env);
          expect(res.body.message).to.equal('API running like a Beast !');
          done();
        })
        .catch(done);
    });
  });
});
