const request = require('supertest-as-promised');
const httpStatus = require('http-status');
const chai = require('chai');
const expect = chai.expect;
const app = require('../../../index');


// initiate user with default values
global.userHook = {
  firstname: 'globalUserHook',
  lastname: 'testLastname',
  email: 'testGlobalMail1@intigo.com',
  password: 'testPassword',
  passwordRepeat: 'testPassword',
};

// create user hook
function userHooks(role = 'ADMIN') {
  describe('# POST /api/users', () => {
    it('should create a new user "HOOK"', (done) => {
      global.userHook.role = role;
      request(app)
        .post('/api/users')
        .send(global.userHook)
        .expect(httpStatus.CREATED)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.message).to.equal('User created Successfully');
          global.userHook._id = res.body.data._id;
          global.userHook.token = res.body.token;
          done();
        })
        .catch(done);
    });
  });
}

// restore user with default values
function restoreUser() {
  global.userHook = {
    firstname: 'globalUserHook',
    lastname: 'testLastname',
    email: 'testGlobalMail@intigo.com',
    phoneNumber: '0000000',
    role: 'ADMIN',
    password: 'testPassword',
    passwordRepeat: 'testPassword',
    mobileDevices: [{
      deviceId: "testId",
      registrationToken: "testToken",
      os: "android-test"
    }]
  };
}

// delete created user hook
function deleteUserHook() {
  describe('# DELETE /api/users', () => {
    it('should delete user "HOOK"', (done) => {
      request(app)
        .delete(`/api/users`)
        .set('authorization', `Bearer ${global.userHook.token}`)
        .send(global.userHook._id)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.message).to.equal('Users have been removed Successfully');
          restoreUser();
          done();
        })
        .catch(done);
    });
  });
}

// fail to delete created user hook
function deleteUserHookFail() {
  describe('# DELETE /api/users', () => {
    it('should delete user "HOOK"', (done) => {
      request(app)
        .delete(`/api/users/${global.userHook._id}`)
        .set('authorization', `Bearer ${global.userHook.token}`)
        .expect(httpStatus.CONFLICT)
        .then((res) => {
          expect(res.body.success).to.equal(false);
          restoreUser();
          done();
        })
        .catch(done);
    });
  });
}

module.exports.userHooks = userHooks;
module.exports.deleteUserHook = deleteUserHook;
module.exports.restoreUser = restoreUser;
module.exports.deleteUserHookFail = deleteUserHookFail;
