const request = require('supertest-as-promised');
const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const chai = require('chai');
const expect = chai.expect;
const app = require('../../index');
const config = require('../../config/config');
const userHooks = require('./global-hooks/user-hooks.test');

chai.config.includeStack = true;

describe('## Weather APIs', () => {

  // create new user "HOOK"
  userHooks.userHooks();

  describe('# Get /api/weather', () => {
    it('should return UNAUTHORIZED when user token missed', (done) => {
      request(app)
        .get('/api/weather?country=Tunisia')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body.success).to.equal(false);
          expect(res.body.message).to.equal('Unauthorized');
          done();
        })
        .catch(done);
    });
  });

  describe('# Get /api/weather', () => {
    it('should return cureent weather data', (done) => {
      request(app)
        .get('/api/weather?country=Tunisia&cacheExpiresSec=1') // set cache expire to 1 second
        .set('Authorization', `Bearer ${global.userHook.token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.weather.location.country).to.equal('Tunisia');
          // Wait 1 second until cache expire
          setTimeout(() => {
            done();
          }, 1000);
        })
        .catch(done);
    });
  });

  describe('# Get /api/weather', () => {
    it('should return fresh weather data', (done) => {
      request(app)
        .get('/api/weather?country=Tunisia')
        .set('Authorization', `Bearer ${global.userHook.token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.cached).to.equal(false);
          expect(res.body.weather.location.country).to.equal('Tunisia');
          done();
        })
        .catch(done);
    });
  });

  describe('# Get /api/weather', () => {
    it('should return cureent weather data', (done) => {
      request(app)
        .get('/api/weather?country=Tunisia')
        .set('Authorization', `Bearer ${global.userHook.token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.weather.location.country).to.equal('Tunisia');
          done();
        })
        .catch(done);
    });
  });

  describe('# Get /api/weather', () => {
    it('should return cached weather data', (done) => {
      request(app)
        .get('/api/weather?country=Tunisia')
        .set('Authorization', `Bearer ${global.userHook.token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.cached).to.equal(true);
          expect(res.body.weather.location.country).to.equal('Tunisia');
          done();
        })
        .catch(done);
    });
  });

  // remove user "HOOK" after executing tests
  userHooks.deleteUserHook();
});
