// const request = require('supertest-as-promised');
// const httpStatus = require('http-status');
// const jwt = require('jsonwebtoken');
// const chai = require('chai');
// const expect = chai.expect;
// const app = require('../../index');
// const config = require('../../config/config');
// const userHooks = require('./global-hooks/user-hooks.test');

// chai.config.includeStack = true;

// describe('## Auth APIs', () => {
//   const validUserLoginCredentials = {
//     email: 'testGlobalMail1@intigo.com',
//     password: 'testPassword',
//   };

//   const wrongUserEmail = {
//     email: 'wrongEmail@intigo.com',
//     password: 'password',
//   };

//   const wrongUserEmailFormat = {
//     email: 'wrongEmailFormat',
//     password: 'password'
//   }

//   const wrongUserPassword = {
//     email: 'testGlobalMail@intigo.com',
//     password: 'wrongPass',
//   };

//   // create new user "HOOK"
//   userHooks.userHooks();

//   describe('# POST /api/users/signin', () => {
//     it('should return UNAUTHORIZED when user email not found', (done) => {
//       request(app)
//         .post('/api/users/signin')
//         .send(wrongUserEmail)
//         .expect(httpStatus.UNAUTHORIZED)
//         .then((res) => {
//           expect(res.body.success).to.equal(false);
//           expect(res.body.message).to.equal('Authentication failed. Wrong credentials');
//           done();
//         })
//         .catch(done);
//     });

//     it('should return BADREQUEST when user email have wrong format', (done) => {
//       request(app)
//         .post('/api/users/signin')
//         .send(wrongUserEmailFormat)
//         .expect(httpStatus.BAD_REQUEST)
//         .then((res) => {
//           expect(res.body.success).to.equal(false);
//           expect(res.body.message).to.equal('\"email\" must be a valid email');
//           done();
//         })
//         .catch(done);
//     });


//     it('should return UNAUTHORIZED when user password is wrong', (done) => {
//       request(app)
//         .post('/api/users/signin')
//         .send(wrongUserPassword)
//         .expect(httpStatus.UNAUTHORIZED)
//         .then((res) => {
//           expect(res.body.success).to.equal(false);
//           expect(res.body.message).to.equal('Authentication failed. Wrong credentials');
//           done();
//         })
//         .catch(done);
//     });

//     it('should login and get valid JWT token', (done) => {
//       request(app)
//         .post('/api/users/signin')
//         .send(validUserLoginCredentials)
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body.success).to.equal(true);
//           expect(res.body).to.have.property('token');
//           jwt.verify(res.body.token, config.jwtSecret, (err, decoded) => {
//             expect(decoded.role).to.equal('ADMIN');
//             done();
//           });
//         })
//         .catch(done);
//     });
//   });
//   // remove user "HOOK" after executing tests
//   userHooks.deleteUserHook();
// });
