const httpStatus = require("http-status");
const _ = require("lodash");

const APIError = require("../helpers/APIError");
const errors = require("../../config/custom-errors");
const userService = require("../services/user.service");
const authService = require("../services/auth.service");
const jwtService = require("../services/jwt.service");

/**
 * @api {get} /api/users Get list of users
 * @apiPrivate
 * @apiName GetListOfUsers
 * @apiGroup Users
 * @apiPermission ADMIN
 * 
 * @apiParam {Number} limit                 Pagination limit
 * @apiParam {String} skip                  Pagination skip
 * @apiParam {String} queries               Filter queries
 * 
 * @apiSuccess {Boolean} success            Specify if request succeded
 * @apiSuccess {Object[]} data              Array of users object
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": []
 *     }
 */
async function getUsers(req, res, next) {
  try {
    const { limit } = req.query;
    const { queries } = req.body;
    const skip = req.query.page * limit;
    let userCount;

    userCount = await userService.count(queries);

    const users = await userService.find(queries, null, '', { limit, skip });
    res.json({
      success: true,
      users,
      userCount,
    });
  } catch (e) {
    next(e);
  }
}

/**
 * @api {post} /api/users Create new user
 * @apiPrivate
 * @apiName PostUser
 * @apiGroup Users
 * @apiPermission ADMIN
 *
 * @apiParam {String} firstname                       Required firstname
 * @apiParam {String} lastname                        Required lastname
 * @apiParam {String} email                           Required email
 * @apiParam {String} password                        Required password
 * @apiParam {String} passwordRepeat                  Required repeat password
 *
 * @apiSuccess {Boolean} success Specify if request succeded
 * @apiSuccess {String} message  Success add message
 * @apiSuccess {Object[]} data   Saved user
 * @apiSuccess {String}   token  Token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "success": true,
 *        "message": 'User created Successfully',
 *        "data": "saved user",
 *        "token": "user token"
 *     }
 */
async function createUser(req, res, next) {
  try {
    // pick only needed fields
    const userData = _.pick(req.body, [
      'firstname',
      'lastname',
      'email',
      'password',
      'role',
    ]);

    // save user in db
    const savedUser = await userService.create(userData);
    // generate auth token
    const token = jwtService.generateAuthToken({
      _id: savedUser._id,
      role: savedUser.role,
    });
    res.status(201).json({
      success: true,
      message: "User created Successfully",
      data: { ...savedUser, },
      token,
      status: 1,
    });
  } catch (e) {
    if (e.message === 'Email already exists') {
      const apiError = new APIError(
        errors.MAIL_ALREADY_EXISTS.message,
        httpStatus.CONFLICT,
        true
      );
      next(apiError);
    } else next(e);
  }
}

/**
 * @api {delete} /api/users Delete all users
 * @apiPrivate
 * @apiName DeleteUsers
 * @apiGroup Users
 * @apiPermission ADMIN
 *
 * @apiParam {Array} usersIds            User or users ids to delete
 * 
 * @apiSuccess {Boolean} success Specify if request succeded
 * @apiSuccess {String} message  Success remove message
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "All users have been removed Successfully"
 *     }
 */
async function removeUsers(req, res, next) {
  const { usersIds } = req.body;
  const query = usersIds && usersIds.lenght ? { _id: { $in: usersIds } } : null;

  try {
    await userService.deleteMany(query);

    res.json({
      success: true,
      message: "Users have been removed Successfully",
    });
  } catch (e) {
    next(e);
  }
}

/**
 * @api {post} /api/auth/login Returns jwt token if valid email and password is provided
 * @apiName PostUserSimpleLogin
 * @apiGroup Auth
 * @apiPermission ADMIN
 * @apiPermission USER
 *
 * @apiParam {String} email    Required user email address
 * @apiParam {String} password Required user password
 *
 * @apiSuccess {Boolean} success Specify if request succeded
 * @apiSuccess {Object} data     Returned loged in user
 * @apiSuccess {string} token    Jwt token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *        "data": {"name": "connected user"},
 *        "token": "userToken"
 *     }
 */
async function login(req, res, next) {
  try {
    const authenticatedUser = await authService.authenticateUserWithCredentials(req.body.email, req.body.password);
    // in case of correct credentials
    if (authenticatedUser) {
      const { data, token } = await authService.generateToken(authenticatedUser);

      return res.json({
        success: true,
        data,
        token,
        status: 1,
      });
    }
    // in case of wrong credentials
    const apiError = new APIError(errors.AUTH_WRONG_CREDENTIALS.message, httpStatus.UNAUTHORIZED, true);
    return next(apiError);
  } catch (e) {
    return next(e);
  }
}

/**
 * @api {get} /api/users Get user
 * @apiPrivate
 * @apiName GetUser
 * @apiGroup Users
 * @apiPermission ADMIN
 * @apiPermission USER
 *
 * @apiSuccess {Boolean} success            Specify if request succeded
 * @apiSuccess {Object[]} data             Array of users object
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": []
 *     }
 */
async function findOne(req, res, next) {
  try {
    const users = await userService.findOne(req.query);

    return res.json({
      success: true,
      users
    });
  } catch (e) {
    next(e);
  }
}

/**
 * @api {put} /api/users:userId Update user
 * @apiPrivate
 * @apiName PutUser
 * @apiGroup Users
 * @apiPermission ADMIN
 * @apiPermission USER
 *
 * @apiParam {String} firstname                       Firstname
 * @apiParam {String} lastname                        Lastname
 * @apiParam {String} email                           Email
 * @apiParam {String} role                            Role
 *
 * @apiSuccess {Boolean} success Specify if request succeded
 * @apiSuccess {String} message  Success update message
 * @apiSuccess {Object[]} data   Saved user
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "success": true,
 *        "message": 'User updated Successfully',
 *        "data": "updated user"
 *     }
 */
async function updateUser(req, res, next) {
  try {
    const updatedUser = await userService.updateOne(req.body, req.loadedUser);
    res.json({
      success: true,
      message: 'User updated Successfully',
      data: updatedUser,
    });
  } catch (e) {
    next(e);
  }
}

/**
 * @api {delete} /api/users/:userId Delete user by Id
 * @apiPrivate
 * @apiName DeleteUser
 * @apiGroup Users
 * @apiPermission ADMIN
 *
 * @apiSuccess {Boolean} success Specify if request succeded
 * @apiSuccess {String} message  Success remove message
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "User removed Successfully"
 *     }
 */
async function remove(req, res, next) {
  try {
    await userService.deleteOne(req.loadedUser._id);

    res.json({
      success: true,
      message: 'User removed Successfully',
    });
  } catch (e) {
    next(e);
  }
}

/**
 * @api {patch} /api/users/:userId/password Change user password
 * @apiPrivate
 * @apiName PatchUserPassword
 * @apiGroup Users
 * @apiPermission ADMIN
 * @apiPermission USER
 *
 * @apiParam {String} oldPassword         Required old password
 * @apiParam {String} newPassword         Required new password
 * @apiParam {String} newPasswordRepeated Required new password repeated
 *
 * @apiSuccess {Boolean} success Specify if request succeded
 * @apiSuccess {String} message  Success update message
 * @apiSuccess {Object} infos    Infos
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "success": true,
 *        "message": "User password updated Successfully",
 *        "infos": "{}"
 *     }
 */
async function changePassword(req, res, next) {
  try {
    await userService.changeUserPassword(
      req.loadedUser,
      req.body.oldPassword,
      req.body.newPassword
    );
    res.json({
      success: true,
      message: 'User password updated Successfully',
    });
  } catch (e) {
    if (e.message === "Passwords do not match") {
      // wrong old password
      const apiError = new APIError(
        errors.WRONG_OLD_PASSWORD.message,
        httpStatus.UNAUTHORIZED
      );
      next(apiError);
    } else next(e);
  }
}

async function loadUser(req, res, next, paramId) {
  try {
    const userFound = await userService.findOne({ _id: paramId });
    if (!userFound) {
      const apiError = new APIError(
        'User not found',
        httpStatus.NOT_FOUND,
        true
      );
      return next(apiError);
    }
    req.loadedUser = userFound;
    return next();
  } catch (e) {
    next(e);
  }
}

module.exports = {
  getUsers,
  createUser,
  removeUsers,
  loadUser,
  login,
  findOne,
  updateUser,
  remove,
  changePassword,
};
