const weatherService = require("../services/weather.service");

/**
 * @api {get} /weather Get current weather
 * @apiPrivate
 * @apiName GetCurrentWeather
 * @apiGroup Weather
 * @apiPermission ADMIN
 * @apiPermission USER
 *
 * @apiSuccess {Boolean} success Specify if request succeded
 * @apiSuccess {Object} weather    Weather
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "success": true,
 *        "weather": "{}"
 *     }
 */
async function getCurrentWeather(req, res, next) {
  const { country = 'Tunisia' } = req.query;
  // For tests purposes (default 300 seconds)
  const { cacheExpiresSec = 300 } = req.query;

  try {
    const weather = await weatherService.getWeather(country, cacheExpiresSec);

    res.json({
      success: true,
      cached: false,
      weather,
    });
  } catch (e) {
    console.log('e:', e.message)
    next(e);
  }
}

module.exports = {
  getCurrentWeather,
};
