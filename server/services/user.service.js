const _ = require('lodash');
const bcrypt = require('bcrypt');
const UserModel = require('../models').User;

// salt used for crypting user password
const SALT_WORK_FACTOR = 10;

async function find(queries = {}, projection, populateParams, params) {
  const { limit, skip } = params;
  const sort = {};
  if (queries.sort) {
    sort[queries.sort.sortBy] = queries.sort.direction === 'asc' ? 1 : -1;
    delete queries.sort;
  }

  try {
    const users = await UserModel
      .find(queries, projection)
      .populate(populateParams || '')
      .limit(limit)
      .skip(skip)
      .sort(sort);

    return users;
  } catch (e) {
    throw e;
  }
}

async function create(userData) {
  try {
    const userFound = await UserModel.findOne({
      email: userData.email,
    });

    if (!!userFound) {
      throw new Error('Email already exists');
    }

    // instanciante new mongoose user
    const user = new UserModel(userData);

    // persist user in db
    let savedUser = await user.save();

    // remove password for safety
    savedUser = savedUser.toObject();
    delete savedUser.password;

    // return savedUser and auth token
    return savedUser;
  } catch (e) {
    throw e;
  }
}

async function findOne(userId) {
  try {
    const userFound = await UserModel.findOne(userId);
    return userFound;
  } catch (e) {
    throw e;
  }
}

async function deleteMany(conditions) {
  try {
    return await UserModel.deleteMany(conditions);
  } catch (e) {
    throw e;
  }
}

async function updateOne(updateData, existingUser) {

  try {
    const userUpdateData = Object.assign({}, updateData);
    // prevent from updating password
    if (userUpdateData.password) delete userUpdateData.password;
    // prevent from updating role
    if (userUpdateData.role) delete userUpdateData.role;

    // merge existing user data with updateData
    const newUser = _.merge(existingUser, userUpdateData);

    const updatedUser = await UserModel.findByIdAndUpdate(
      newUser._id,
      newUser,
      {
        new: true,
      }
    );

    return updatedUser;
  } catch (e) {
    throw e;
  }
}

async function changeUserPassword(existingUser, oldPass, newPass) {
  try {
    const isMatching = await existingUser.comparePasswordAsync(oldPass);
    if (isMatching) {
      bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
        if (err) throw err;
        // hash the password using the new salt
        bcrypt.hash(newPass, salt, async (error, hash) => {
          if (error) throw error;
          // override the cleartext password with the hashed one
          await UserModel.updateOne(
            {
              _id: existingUser._id,
            },
            {
              $set: {
                password: hash,
              },
            }
          );
        });
      });
    } else {
      throw new Error('Passwords do not match');
    }
  } catch (e) {
    throw e;
  }
}

async function count(query) {
  try {
    const users = await UserModel.countDocuments(query);

    return users;
  } catch (e) {
    throw e;
  }
}

module.exports = {
  find,
  create,
  deleteMany,
  findOne,
  updateOne,
  changeUserPassword,
  count,
};
