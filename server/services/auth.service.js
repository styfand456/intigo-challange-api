const UserModel = require('../models').User;

const jwtService = require('./jwt.service');

async function authenticateUserWithCredentials(email, password) {
  try {
    const userFound = await UserModel.findOne({
      email
    }).populate('corp');
    if (userFound && userFound.password) {
      const isMatching = await userFound.comparePasswordAsync(password);
      if (isMatching) {
        return userFound;
      }
    }
    return null;
  } catch (e) {
    throw e;
  }
}

async function generateToken(authenticatedUser) {
  try {
    const id = authenticatedUser.role === 'PRODUCER' ? authenticatedUser.producer : authenticatedUser._id;

    const __authenticatedUser = authenticatedUser.toObject();
    delete __authenticatedUser.password;
    // generate auth token
    const token = jwtService.generateAuthToken({
      _id: id,
      role: __authenticatedUser.role
    });

    return { data: __authenticatedUser, token };
  } catch (e) {
    throw e;
  }
}

module.exports = {
  authenticateUserWithCredentials,
  generateToken
};
