const jwt = require('jsonwebtoken');
const config = require('../../config/config');

function generateAuthToken(tokenData) {
  try {
    return jwt.sign(tokenData, config.jwtSecret, { expiresIn: '60 days' });
  } catch (e) {
    throw e;
  }
}

module.exports = {
  generateAuthToken
};

