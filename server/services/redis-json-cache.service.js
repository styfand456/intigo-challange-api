const redis = require('redis');
const config = require('../../config/config');

const client = redis.createClient({ url: `redis://${config.redis.host}:${config.redis.port}` });
client.on('error', (err) => {
  throw err;
});
client.connect('error', (err) => {
  throw err;
});

module.exports = class RedisJsonCacheService {
  /**
   * Save new data in redisJson
   * @param {astring} key: The key for data to save (same data should have same prefix)
   * @param {any} value: The data to save
   * @param {number} expirationSec: The data will be expired after x seconds
   */
  static saveData(key, value, expirationSec) {
    try {
      return new Promise(async (resolve) => {
        await client.set(key, JSON.stringify(value), {
          EX: expirationSec,
        });
        resolve();
      });
    } catch (error) {
      throw error;
    }
  }

  /**
   * Get data from redisJson by key
   * @param {string} key
   * @param {any} value
   */
  static savePermanentData(key, value) {
    try {
      return new Promise(async (resolve) => {
        await client.set(key, JSON.stringify(value));
        resolve();
      });
    } catch (error) {
      throw error;
    }
  }

  /**
   * Get data from redisJson by key
   * @param {string} key
   */
  static async getData(key) {
    try {
      return new Promise(async (resolve) => {
        const data = await client.get(key);
        resolve(JSON.parse(data));
      });
    } catch (error) {
      throw error;
    }
  }
};
