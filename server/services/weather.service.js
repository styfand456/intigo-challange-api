const axios = require('axios');
const config = require('../../config/config');
const redisJsonCacheService = require('../services/redis-json-cache.service');

async function getWeather(country, expirationSec) {
  try {
    const { data } = await axios.get(`${config.weatherApiHost}/current.json?q=${country}&key=${config.weatherApiKey}`);
    // Save data in redis cache for use it later if still valid (deafult expire in 5 Minutes)
    await redisJsonCacheService.saveData(country, JSON.stringify(data), 1);

    return { ...data };
  } catch (e) {
    throw e;
  }
}

module.exports = {
  getWeather,
};
