const express = require('express');
const validate = require('express-validation');
const expressJwt = require('express-jwt');

const config = require('../../config/config');
const useCache = require('../middlewares/cache.middlewar');
const weatherCtrl = require('../controllers/weather.controlle');
const validateRoles = require('../middlewares/validate-roles.middleware');

const router = express.Router();

router.route('/')
  /** GET /api/weather - Get current weather */
  .get(expressJwt({
    secret: config.jwtSecret,
  }), validateRoles(['ADMIN']), useCache, weatherCtrl.getCurrentWeather)

module.exports = router;
