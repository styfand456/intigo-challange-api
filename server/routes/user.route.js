const express = require('express');
const validate = require('express-validation');
const expressJwt = require('express-jwt');

const config = require('../../config/config');
const paramValidation = require('../../config/param-validation');
const userCtrl = require('../controllers/user.controller');
const validateRoles = require('../middlewares/validate-roles.middleware');

const router = express.Router();

router.route('/')
  /** GET /api/users - Get list of users */
  .get(expressJwt({
    secret: config.jwtSecret,
  }), validateRoles(['ADMIN']), userCtrl.getUsers)

  /** POST /api/users - Create new user */
  .post(validate(paramValidation.signup), userCtrl.createUser)

  /** DELETE /api/users - Delete all users (for Admin purpose only) */
  .delete(expressJwt({
    secret: config.jwtSecret,
  }), validateRoles(['ADMIN']), userCtrl.removeUsers);

router.route('/signin')
  /** POST /api/users/signin - Signin user */
  .post(validate(paramValidation.login), userCtrl.login);

router.route('/:userId')
  /** GET /api/users/:userId - Get user */
  .get(expressJwt({
    secret: config.jwtSecret,
  }), validateRoles(['ADMIN']), userCtrl.findOne)

  /** PUT /api/users/:userId - Update user */
  .put(
    expressJwt({
      secret: config.jwtSecret,
    }),
    validateRoles(['ADMIN']),
    validate(paramValidation.updateUser),
    userCtrl.updateUser,
  )

// ************************ USER PASSWORD ******************************************

router.route('/:userId/password')
  /** PATCH /api/users/:userId/password - Change user password */
  .patch(
    expressJwt({
      secret: config.jwtSecret,
    }), validateRoles(['ADMIN', 'USER']),
    validate(paramValidation.changePassword),
    userCtrl.changePassword,
  );

/** Load user when API with userId route parameter is hit */
router.param('userId', userCtrl.loadUser);

module.exports = router;
