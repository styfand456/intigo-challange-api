const express = require('express');
const url = require('url');
const mongoose = require('mongoose');
const httpStatus = require('http-status');

const userRoutes = require('./user.route');
const weatherRoutes = require('./weather.route');
const config = require('../../config/config');
const APIError = require('../helpers/APIError');

const pjson = require('../../package.json');

const router = express.Router();

const dbStatus = [{
  value: 0,
  description: 'Disconnected'
},
{
  value: 1,
  description: 'Connected'
},
{
  value: 2,
  description: 'Connecting'
},
{
  value: 3,
  description: 'Disconnecting'
}
];

// check server an db status
router.get('/health-check', async (req, res, next) => {
  const hostname = req.headers.host;
  const {
    pathname
  } = url.parse(req.url);
  const dbStatusRes = await dbStatus
    .find(status => status.value === mongoose.connection.readyState);
  if (dbStatusRes.description !== 'Connected') {
    const apiError = new APIError('Internal server problem', httpStatus.INTERNAL_SERVER_ERROR);
    return next(apiError);
  }
  res.json({
    success: true,
    env: config.env,
    hostname,
    pathname,
    version: pjson.version,
    dbStatus: dbStatusRes.description,
    message: 'API running like a Beast !',
  });
});

// mount user routes at /users
router.use('/users', userRoutes);

// mount weather route at /weather
router.use('/weather', weatherRoutes);

module.exports = router;
