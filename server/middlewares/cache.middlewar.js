const redisJsonCacheService = require('../services/redis-json-cache.service');

module.exports = async function (req, res, next) {
  const { country } = req.query;

  try {
    const result = await redisJsonCacheService.getData(country);
    if (!!result) {
      const cachedData = JSON.parse(result);
      const data = { weather: cachedData };
      data.success = true;

      // For tests purposes
      data.cached = true;

      return res.json(data);
    } else {
      return next();
    }

  } catch (error) {
    throw error;
  }
};
