const _ = require('lodash');
const APIError = require('../APIError');

module.exports = function validateRoles(roles) {
  const allowMultipleRoles = _.isArray(roles);
  return (req, res, next) => {
    if (allowMultipleRoles) {
      if (_.includes(roles, req.user.role)) {
        return next();
      }
    }
    if (roles === req.user.role) {
      return next();
    }
    const err = new APIError('Acces denied permissions required', 401, true);
    return next(err);
  };
};
