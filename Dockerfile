# take node image
FROM node:14.15.0

## The one to blame
LABEL maintainer="Sofiene DIMASSI <dimessisofiene@gmail.com>"

## How to run this file
LABEL cmd="docker build -t <image name> . (sudo permission required for mac/linux)"
LABEL cmd="docker run -d -p 4042:4042 <image name> . (sudo permission required for mac/linux)"

# set /app directory as default working directory
WORKDIR /app

# Set environment variables from this file
ENV env_file=".env"

# only copy package.json initially so that `RUN yarn` layer is recreated only
# if there are changes in package.json
ADD package.json yarn.lock /app/

# --pure-lockfile: Don’t generate a yarn.lock lockfile
RUN yarn --pure-lockfile

# install app dependencies
RUN yarn install

# copy all file from current dir to /app in container
COPY . .

# Re-installe bcrypt to prevent OS comtability isseus
RUN yarn remove bcrypt
RUN yarn add bcrypt@3.0.6

# expose port 4042
EXPOSE 4042

# cmd to start service
CMD [ "yarn", "start" ]
